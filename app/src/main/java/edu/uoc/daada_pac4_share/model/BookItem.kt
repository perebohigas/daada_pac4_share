package edu.uoc.daada_pac4_share.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.annotation.NonNull

@Entity(tableName = "book_table")
data class BookItem (

    @PrimaryKey
    @NonNull
    val identifier: Int,
    @NonNull
    val title: String,
    @NonNull
    val author: String,
    @ColumnInfo(name = "publication_date")
    val publicationDate: String,
    val description: String,
    @ColumnInfo(name = "url_image")
    val imageURL: String

) {

    override fun toString(): String {
        return "[ID: ${identifier}, TITLE: ${title}, AUTHOR: ${author}, PUBLICATION DATE: ${publicationDate}]"
    }

    override fun equals(other: Any?): Boolean {
        if (other is BookItem) {
            if (this.title.equals(other.title) && this.author.equals(other.author) && this.publicationDate.equals(publicationDate)) {
                return true
            }
        }
        return false
    }
}
