package edu.uoc.daada_pac4_share

import android.net.Uri
import android.os.Bundle
import android.view.View
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity
import edu.uoc.daada_pac4_share.model.BookContent
import edu.uoc.daada_pac4_share.model.BookDao
import edu.uoc.daada_pac4_share.model.BookItem
import kotlinx.android.synthetic.main.activity_book_detail.*
import android.widget.TextView
import android.webkit.WebView
import android.webkit.WebViewClient
import com.google.android.material.floatingactionbutton.FloatingActionButton
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class BookDetailActivity : AppCompatActivity() {

    private lateinit var book: BookItem
    private lateinit var webView: WebView
    private lateinit var fab: FloatingActionButton

    // Instances of Room
    private lateinit var roomDatabase: BookContent
    private lateinit var bookDAO: BookDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_detail)

        roomDatabase = BookContent.getAppDataBase(this.applicationContext)!!
        bookDAO = roomDatabase.bookDataAccesObject()

        setSupportActionBar(toolbar)

// ===================================== START OF CODE TO BE COMPLETED - EXERCISE 3 ====================================
        webView = this.findViewById(R.id.web_view)

        fab = this.findViewById(R.id.fab)
        fab.setOnClickListener { view ->
            (fab as View).visibility = View.INVISIBLE
            webView.loadUrl("file:///android_asset/form.html")
            webView.settings.javaScriptEnabled = true
            webView.requestLayout()
            webView.visibility = View.VISIBLE
            webView.setWebViewClient(object : WebViewClient() {

                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    val uri = Uri.parse(url)
                    return checkData(uri)
                }

                private fun checkData(uri: Uri): Boolean {
                    val name = uri.getQueryParameter("name")
                    val num = uri.getQueryParameter("num")
                    val date = uri.getQueryParameter("date")
                    val format: DateFormat = SimpleDateFormat("dd/MM/yyyy")
                    format.isLenient = false
                    if (name != null && name != "" && num != null && num != "" && date != null && date != "") {
                        try {
                            val parsedDate: Date = format.parse(date)
                            Snackbar.make(view, resources.getString(R.string.form_succes), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show()
                            webView.visibility = View.INVISIBLE
                            fab.show()
                        } catch (e: ParseException){
                            Snackbar.make(view, resources.getString(R.string.form_date_error), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show()
                        }
                    } else {
                        Snackbar.make(view, resources.getString(R.string.form_error), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                    }
                    return true
                }
            })


        }
// ====================================== END OF CODE TO BE COMPLETED - EXERCISE 3 =====================================

        // Add an upper arrow to return to activity BookListActivity (defined in the manifest)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val bookIdentifier: Int = intent.getIntExtra("BOOK_ITEM_IDENTIFIER", 0)


        book = bookDAO.getBook(bookIdentifier) ?: bookDAO.getAllBooks().first()

        var authorTextView = findViewById(R.id.author) as TextView
        authorTextView.text = book.author
        var publicationDateTextView = findViewById(R.id.publicationDate) as TextView
        publicationDateTextView.text = book.publicationDate
        var descriptionTextView = findViewById(R.id.description) as TextView
        descriptionTextView.text = book.description
    }
}
