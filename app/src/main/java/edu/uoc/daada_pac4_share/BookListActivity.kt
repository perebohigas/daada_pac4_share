package edu.uoc.daada_pac4_share

import android.app.AlertDialog
import android.app.NotificationManager
import android.content.*
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log
import android.view.*
import android.webkit.WebView
import android.webkit.WebViewClient
import android.widget.TextView
import android.widget.Toast
import co.zsmb.materialdrawerkt.builders.accountHeader
import co.zsmb.materialdrawerkt.builders.drawer
import co.zsmb.materialdrawerkt.builders.footer
import co.zsmb.materialdrawerkt.draweritems.badgeable.primaryItem
import co.zsmb.materialdrawerkt.draweritems.divider
import co.zsmb.materialdrawerkt.draweritems.profile.profile
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.database.*
import com.google.firebase.iid.FirebaseInstanceId

import kotlinx.android.synthetic.main.activity_book_list.*
import kotlinx.android.synthetic.main.book_list.*

import edu.uoc.daada_pac4_share.model.BookContent
import edu.uoc.daada_pac4_share.model.BookDao
import edu.uoc.daada_pac4_share.model.BookItem
import kotlinx.android.synthetic.main.book_list_even_element.view.*
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.io.OutputStream
import java.text.DateFormat
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*

class BookListActivity: AppCompatActivity() {

    private var bookList = emptyList<BookItem>()
    private var twoPanelMode: Boolean = false
    private var recyclerAdapter: SimpleItemRecyclerViewAdapter? = null

    // Purchase
    private lateinit var webView: WebView
    private lateinit var fab: FloatingActionButton

    // User's account
    val username = "Pere Bohigas"
    val userEmail = "perebohigas@uoc.edu"

    // Variables for Firebase
    val passwordFirebase = "123456"

    // Instances of FirebaseAuth and FirebaseDatabase
    private lateinit var firebaseAuthorization: FirebaseAuth
    private lateinit var firebaseDatabase: FirebaseDatabase
    private lateinit var firebaseMessagingToken: String

    // Instances of Room
    private lateinit var roomDatabase: BookContent
    private lateinit var bookDAO: BookDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_book_list)

        initializeDataBase()
        initializeFirebase()

        if (firebaseAuthorization.currentUser == null) {
            loginInFirebase()
        }

        twoPanelMode = checkIfFrameForBookDetailIsPresent()
        setActionBar()
        if (twoPanelMode) {
            setPurchaseFeature()
        }
        initializeRecyclerView()

        loadBooksFromRoomDatabase()

        catchInitialIntentAndProceed()

        setDrawer()
    }

// =====================================================================================================================
// =================================================== PURCHASE FORM ===================================================
// =====================================================================================================================

// ===================================== START OF CODE TO BE COMPLETED - EXERCISE 3 ====================================
    fun setPurchaseFeature() {

        webView = this.findViewById(R.id.web_view)
        fab = this.findViewById(R.id.fab)
        fab.show()
        fab.setOnClickListener { view ->
            fab.hide()
            webView.loadUrl("file:///android_asset/form.html")
            webView.settings.javaScriptEnabled = true
            webView.requestLayout()
            webView.visibility = View.VISIBLE
            webView.setWebViewClient(object : WebViewClient() {

                override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
                    val uri = Uri.parse(url)
                    return checkData(uri)
                }

                private fun checkData(uri: Uri): Boolean {
                    val name = uri.getQueryParameter("name")
                    val num = uri.getQueryParameter("num")
                    val date = uri.getQueryParameter("date")
                    val format: DateFormat = SimpleDateFormat("dd/MM/yyyy")
                    format.isLenient = false
                    if (name != null && name != "" && num != null && num != "" && date != null && date != "") {
                        try {
                            val parsedDate: Date = format.parse(date)
                            Snackbar.make(view, resources.getString(R.string.form_succes), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show()
                            webView.visibility = View.INVISIBLE
                            fab.show()
                        } catch (e: ParseException){
                            Snackbar.make(view, resources.getString(R.string.form_date_error), Snackbar.LENGTH_LONG)
                                .setAction("Action", null).show()
                        }
                    } else {
                        Snackbar.make(view, resources.getString(R.string.form_error), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                    }
                    return true
                }
            })

        }
    }
// ====================================== END OF CODE TO BE COMPLETED - EXERCISE 3 =====================================

// =====================================================================================================================
// =================================================== NOTIFICATIONS ===================================================
// =====================================================================================================================

    fun catchInitialIntentAndProceed() {
        if (intent != null) {
            // The App was intialized from an Intent
            if (intent.action != null) {
                // The Intent has an associated action
                if (intent.hasExtra(getString(R.string.intent_data_book_identifier))) {
                    // The Intent has a book identifier and this could be parsed to an int
                    val bookIdentifierFromIntent: Int = intent.getIntExtra(getString(R.string.intent_data_book_identifier), -1)
                    when (intent.action) {
                        getString(R.string.action_show_details) -> {
                            Log.i(getString(R.string.app_name),"App started from a notification with the action to show the details of the book with the id [${bookIdentifierFromIntent}]")
                            showBookDetails(bookIdentifierFromIntent)
                        }
                        getString(R.string.action_delete_book_locally) -> {
                            Log.i(getString(R.string.app_name),"App started from a notification with the action to delete the book with the id [${bookIdentifierFromIntent}]")
                            bookList.forEach {
                                if (it.identifier.equals(bookIdentifierFromIntent)){
                                    deleteBookLocally(it)
                                }
                            }
                        }
                    }
                }
            } else {
                Log.i(getString(R.string.app_name),"App started from a notification without any selected action. It would start normally.")
            }
            // Delete all notifications from this App
            val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.cancelAll()
        }
    }

// =====================================================================================================================
// ============================================ BACKEND DATABASE (FIREBASE) ============================================
// =====================================================================================================================

    fun initializeFirebase() {
        // Initialize instances of FirebaseAuth and FirebaseDatabase
        firebaseAuthorization = FirebaseAuth.getInstance()
        firebaseDatabase = FirebaseDatabase.getInstance()
        retrieveCurrentFirebaseCloudMessagingToken()
    }

    fun loginInFirebase() {
        firebaseAuthorization.signInWithEmailAndPassword(userEmail, passwordFirebase)
            .addOnCompleteListener(this) { task ->
                val toast: Toast
                if (task.isSuccessful) {
                    // Sign in succeeded
                    val user = firebaseAuthorization.currentUser
                    downloadBooksFromFirebase()
                    Log.i(getString(R.string.app_name), "Authentication with Firebase success with email [${user!!.email}]")

                    toast = Toast.makeText(baseContext, getString(R.string.authentication_message_success), Toast.LENGTH_SHORT)
                    toast.setGravity(Gravity.TOP, 0, 0)
                } else {
                    // Sign in failed
                    Log.w(getString(R.string.app_name), "Authentication with Firebase failure", task.exception)
                    toast = Toast.makeText(baseContext, getString(R.string.authentication_message_fail), Toast.LENGTH_SHORT)
                }
                // Display a toast message to the user with the result of the authentication
                toast.show()
            }
    }

    fun retrieveCurrentFirebaseCloudMessagingToken() {
        FirebaseInstanceId.getInstance().instanceId
            .addOnCompleteListener(OnCompleteListener { task ->
                if (!task.isSuccessful) {
                    Log.w(getString(R.string.app_name), "getInstanceId failed", task.exception)
                    return@OnCompleteListener
                }

                // Get new Instance ID token
                firebaseMessagingToken = task.result?.token!!

                // Show current token in Log
                Log.i(getString(R.string.app_name), "The current Firebase Cloud Messaging token is: ${firebaseMessagingToken}")
            })
    }

    fun downloadBooksFromFirebase() {
        if (firebaseAuthorization.currentUser != null) {
            var reference: DatabaseReference = firebaseDatabase.getReference("books")

            reference.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    loadBooksFromDataSnapshot(dataSnapshot)
                    Log.i(
                        getString(R.string.app_name),
                        "Books from Firebase database loaded successfully, and new ones added in local database"
                    )
                }

                override fun onCancelled(error: DatabaseError) {
                    Log.w(
                        getString(R.string.app_name),
                        "Error loading books list from Firebase database, showing only books from local database"
                    )
                }
            })
        }
    }

    private fun loadBooksFromDataSnapshot(dataSnapshot: DataSnapshot) {
        // Function to parse BookItem objects from a Firebase database DataSnapshot

        for (childDataSnapshot in dataSnapshot.getChildren()) {
            val identifier: Int = (childDataSnapshot.getKey() as String).toInt()
            val title: String = childDataSnapshot.child("title").getValue() as String
            val author: String = childDataSnapshot.child("author").getValue() as String
            val publicationDate: String = childDataSnapshot.child("publication_date").getValue() as String
            val description: String = childDataSnapshot.child("description").getValue() as String
            val imageURL: String = childDataSnapshot.child("url_image").getValue() as String

            val book = BookItem(identifier, title, author, publicationDate, description, imageURL)
            loadNewBookInRoomDatabase(book)
        }
    }

    private fun refreshBookListFromFirebase() {
        var reference: DatabaseReference = firebaseDatabase.getReference("books")
        reference.addListenerForSingleValueEvent(object : ValueEventListener {
            override fun onDataChange(dataSnapshot: DataSnapshot) {
                loadBooksFromDataSnapshot(dataSnapshot)
                loadBooksFromRoomDatabase()
            }

            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }

// =====================================================================================================================
// =============================================== LOCAL DATABASE (ROOM) ===============================================
// =====================================================================================================================

    fun initializeDataBase() {
        // Initialize Room Database
        roomDatabase = BookContent.getAppDataBase(this)!!
        bookDAO = roomDatabase.bookDataAccesObject()
    }

    fun loadBooksFromRoomDatabase() {
        bookList = bookDAO.getAllBooks()
        cleanDuplicatesInRoomDatabase()
        bookList.sortedBy { it.identifier }
        recyclerAdapter!!.setItems(bookList)
    }

    fun loadNewBookInRoomDatabase(newBook: BookItem) {
        if (bookDAO.getDuplicates(newBook.title, newBook.author, newBook.publicationDate).isNullOrEmpty()) {
            bookDAO.addBook(newBook)
            Log.i(getString(R.string.app_name), "Added Book ${newBook} into Room Database")
        }
    }

    fun deleteBookLocally(book: BookItem) {
        bookDAO.deleteBook(book)
        loadBooksFromRoomDatabase()
    }

    fun cleanDuplicatesInRoomDatabase() {
        bookList.forEach {
            if (bookDAO.getDuplicates(it.title, it.author, it.publicationDate)!!.count() > 1) {
                bookDAO.deleteBook(it)
            }
        }
    }


// =====================================================================================================================
// ================================================== SHARING OPTIONS ==================================================
// =====================================================================================================================

// ===================================== START OF CODE TO BE COMPLETED - EXERCISE 2 ====================================
    fun shareToOtherApps() {
        val shareIntent = Intent().apply {
            action = Intent.ACTION_SEND
            flags = Intent.FLAG_ACTIVITY_NEW_TASK
            putExtra(Intent.EXTRA_TEXT, getString(R.string.shared_text))
            val uriToImage = saveImageToInternalStorage(R.mipmap.book_symbol)
            if (uriToImage != null) {
                type = "image/png"
                putExtra(Intent.EXTRA_STREAM, uriToImage)
            }
        }
        startActivity(Intent.createChooser(shareIntent, resources.getText(R.string.chooser_text)))
    }

    fun copyToClipboard() {
        val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip: ClipData = ClipData.newPlainText(resources.getString(R.string.copy_label), resources.getString(R.string.shared_text))
        clipboard.primaryClip = clip
        val builder: AlertDialog.Builder = this@BookListActivity.let {
            AlertDialog.Builder(it)
                .setMessage("\"${resources.getString(R.string.shared_text)}\"")
                .setTitle(resources.getString(R.string.copy_message))
                .setNeutralButton(resources.getString(R.string.copy_button_text), null)
        }
        builder.create()
            .show()
    }

    fun shareToWhatsApp() {
        val packageName = "com.whatsapp"
        val isWhatsAppInstalled = isPackageInstalled(packageName)
        if (isWhatsAppInstalled) {
            val whatsAppIntent = Intent().apply {
                action = Intent.ACTION_SEND
                flags = Intent.FLAG_GRANT_READ_URI_PERMISSION
                putExtra(Intent.EXTRA_TEXT, getString(R.string.shared_text))
                setPackage(packageName)
                val uriToImage = saveImageToInternalStorage(R.mipmap.book_symbol)
                if (uriToImage != null) {
                    type = "image/png"
                    putExtra(Intent.EXTRA_STREAM, uriToImage)
                }
            }
            startActivity(whatsAppIntent)
        }
    }

    private fun saveImageToInternalStorage(drawableId: Int): Uri? {

        // Create a bitmap from the drawable object
        val bitmap = BitmapFactory.decodeResource(resources, drawableId)
        // Initializing a new file, getting a directory in internal storage
        val imageFile = File(getExternalFilesDir(Environment.DIRECTORY_PICTURES), "${resources.getString(R.string.app_name)}_icon.png")

        try {
            // Get the file output stream
            val stream: OutputStream = FileOutputStream(imageFile)

            // Compress bitmap
            bitmap.compress(Bitmap.CompressFormat.PNG, 80, stream)

            // Flush the stream
            stream.flush()

            // Close stream
            stream.close()
        } catch (error: IOException){ // Catch the exception
            Log.e(getString(R.string.app_name), "Error while compressing the image to share")
            return null
        }

        // Return the saved image uri
        return Uri.parse(imageFile.absolutePath)
    }

    private fun isPackageInstalled(packageName: String): Boolean {
        try {
            return packageManager.getApplicationInfo(packageName, 0).enabled
        }
        catch (error: PackageManager.NameNotFoundException) {
            return false
        }
    }
// ====================================== END OF CODE TO BE COMPLETED - EXERCISE 2 =====================================

// =====================================================================================================================
// =================================================== USER INTERFACE ==================================================
// =====================================================================================================================

// ===================================== START OF CODE TO BE COMPLETED - EXERCISE 1 ====================================
    fun setDrawer() {
        drawer{

            showOnFirstLaunch = true
            closeOnClick = false

            accountHeader {
                profile(username, userEmail) {
                    icon = R.drawable.profile_picture
                }
                translucentStatusBar = true
                background = R.color.primary
                textColorRes = R.color.colorText
            }
            primaryItem (getString(R.string.action_share_content)) {
                icon = R.drawable.ic_share_black_24dp
                selected = false
                selectable = false
                onClick { _ ->
                    shareToOtherApps()
                    Log.d(getString(R.string.app_name), "Drawer: Sharing to other apps")
                    false
                }
            }
            primaryItem(getString(R.string.action_copy)) {
                icon = R.drawable.ic_content_copy_black_24dp
                selected = false
                selectable = false
                onClick { _ ->
                    copyToClipboard()
                    Log.d(getString(R.string.app_name), "Drawer: Text copied to clipboard")
                    false
                }
            }
            primaryItem(getString(R.string.action_share_whatsapp)) {
                icon = R.drawable.ic_chat_bubble_outline_black_24dp
                selected = false
                selectable = false
                onClick { _ ->
                    shareToWhatsApp()
                    Log.d(getString(R.string.app_name), "Drawer: Sharing to WhatsApp")
                    false
                }
            }
            divider {}
            footer {
                primaryItem(getString(R.string.action_sort_author)) {
                    onClick { _ ->
                        sortBooksByAuthor()
                        Log.d(getString(R.string.app_name), "Drawer: Books sorted by author")
                        false
                    }
                    selectable = true
                }
                primaryItem(getString(R.string.action_sort_title)) {
                    onClick { _ ->
                        sortBooksByTitle()
                        Log.d(getString(R.string.app_name), "Drawer: Books sorted by title")
                        false
                    }
                    selectable = true
                }
            }
        }
    }

    fun sortBooksByTitle() {
        recyclerAdapter?.setItems(roomDatabase.bookDataAccesObject().getAllBooks().sortedWith(compareBy({ it.title })))
    }

    fun sortBooksByAuthor() {
        recyclerAdapter?.setItems(roomDatabase.bookDataAccesObject().getAllBooks().sortedWith(compareBy({ it.author })))
    }
// ====================================== END OF CODE TO BE COMPLETED - EXERCISE 1 =====================================

    fun initializeRecyclerView() {
        var linearLayoutManager = androidx.recyclerview.widget.LinearLayoutManager(this)
        val recyclerView: androidx.recyclerview.widget.RecyclerView = findViewById(R.id.list_of_books)
        recyclerView.layoutManager = linearLayoutManager
        recyclerAdapter = SimpleItemRecyclerViewAdapter(bookList, this)
        recyclerView.adapter = recyclerAdapter
    }

    fun checkIfFrameForBookDetailIsPresent(): Boolean {
        if (frame_for_book_details == null) {
            Log.i(getString(R.string.app_name),"Book details is NOT been shown [small screen]")
            return false
        } else {
            Log.i(getString(R.string.app_name),"Book details is been shown [big screen]")
            return true
        }
    }

    class SimpleItemRecyclerViewAdapter(private var listBookItems: List<BookItem>, private val context: Context) : androidx.recyclerview.widget.RecyclerView.Adapter<SimpleItemRecyclerViewAdapter.ViewHolder>() {
        private val EVEN = 0
        private val ODD = 1

        override fun getItemCount(): Int {
            return listBookItems.size
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            holder.mItem = listBookItems[position]
            holder.mTitleView.text = listBookItems[position].title
            holder.mAuthorView.text = listBookItems[position].author

            // Saves the current position of the view's holder
            var bookItemPosition = position

            holder.itemView.setOnClickListener { _ ->
                var currentPosition: Int

                // Get the position of the book which was clicked in the view
                currentPosition = bookItemPosition

                val selectedBook: BookItem = listBookItems[currentPosition]

                Log.i(context.getString(R.string.app_name), "The book ${selectedBook} in the position ${currentPosition} has been clicked")

                if (context is BookListActivity) {context.showBookDetails(selectedBook.identifier)}
            }
        }

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
            var view: View? = null

            // Load a diferent layout for even and odd elements
            if (viewType == EVEN) {
                view = LayoutInflater.from(parent.context).inflate(R.layout.book_list_even_element, parent, false)
            } else if (viewType == ODD) {
                view = LayoutInflater.from(parent.context).inflate(R.layout.book_list_odd_element, parent, false)
            }
            return ViewHolder(view!!)
        }

        override fun getItemViewType(position: Int): Int {
            // Return type even or odd according to its position
            var type: Int
            if (position % 2 == 0) {
                type = EVEN
            } else {
                type = ODD
            }
            return type
        }

        class ViewHolder(elementView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(elementView) {
            lateinit var mItem: BookItem
            val mTitleView: TextView = elementView.titleView
            val mAuthorView: TextView = elementView.authorView
        }

        fun setItems(bookItems: List<BookItem>) {
            listBookItems = bookItems
            notifyDataSetChanged()
        }
    }

    fun showBookDetails(bookIdentifier: Int) {
        if(twoPanelMode) {
            // Start the fragment corresponding to a tablet, sending as argument the selected position
            val newFragment = BookDetailFragment().apply {
                arguments = Bundle()
                arguments!!.putInt("BOOK_ITEM_IDENTIFIER", bookIdentifier)
            }
            Log.i(getString(R.string.app_name), "Add fragment to this activity to show the details of the book with the identifier ${bookIdentifier}")
            supportFragmentManager.beginTransaction()
                .replace(R.id.frame_for_book_details, newFragment)
                .commit()
        } else {
            // Start the activity corresponding to a smartphone, sending as argument the selected position
            Log.i(getString(R.string.app_name), "Start BookDetailsActivity to show the details of the book with the identifier ${bookIdentifier}")
            val intent = Intent(this, BookDetailActivity::class.java)
            intent.putExtra("BOOK_ITEM_IDENTIFIER", bookIdentifier)
            startActivity(intent)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        val inflater = menuInflater
        inflater.inflate(R.menu.menu_book_list, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.download_books_from_firebase) {
            refreshBookListFromFirebase()
            return true
        } else {
            return super.onOptionsItemSelected(item)
        }
    }

    fun setActionBar() {
        setSupportActionBar(toolbar)
    }
}
